<?php

namespace App\Http\Controllers;

use App\Http\Requests\UploadCsvRequest;
use App\Interfaces\ICsvUploaderService;
use App\Interfaces\Repositories\IStaffRepository;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    const UPLOAD_DIR = 'upload';

    /**
     * The staff repository instance.
     *
     * @var IStaffRepository $staff
     */
    protected $staff;


    /**
     * Uploader service
     *
     * @var ICsvUploaderService $uploaderService
     */
    protected $uploaderService;

    /**
     * HomeController constructor.
     * @param IStaffRepository $staffRepository
     */
    public function __construct(
        IStaffRepository $staffRepository,
        ICsvUploaderService $uploaderService
    ) {
        $this->staff = $staffRepository;
        $this->uploaderService = $uploaderService;
    }

    /**
     * Index action.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $staff = $this->staff->search($request);

        return view('home.index', array_merge([
            'staff' => $staff,
            'searchParams' => $request->all()
        ]));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function save(UploadCsvRequest $request)  // toDo if time add request filter
    {
        $fileName = '';

        if ($request->hasFile('csv_file')) {
            $fileName = $request->file('csv_file')->getClientOriginalName();
            $request->file('csv_file')->move(self::UPLOAD_DIR, $fileName);
        }

        try {
            $this->uploaderService->upload(self::UPLOAD_DIR . '/' . $fileName);
            return redirect()->back();
        } catch (\Exception $e) {
            echo 'Error happened: ',  $e->getMessage(), "\n";
        }
    }
}
