<?php

namespace App\Interfaces;

interface ICsvUploaderService
{
    /**
     * @param $fileName
     * @return bool
     */
    public function upload($fileName);
}