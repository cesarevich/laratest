<?php

namespace App\Interfaces\Repositories;

use App\Models\Staff;

interface IStaffRepository
{
    /**
     * Search through Staff model
     * @return mixed
     */
    public function search();
}