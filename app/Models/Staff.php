<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Staff
 */
class Staff extends Model
{
    const IS_NOT_EDITABLE = 0;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name',
        'last_name',
        'birthdate',
        'email',
        'home_city',
        'home_zip',
        'home_address',
        'phone',
        'company_name',
        'work_city',
        'work_address',
        'position',
        'cv'
    ];
}

