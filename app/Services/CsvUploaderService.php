<?php

namespace App\Services;

use App\Interfaces\ICsvUploaderService;
use App\Models\Staff;
use Illuminate\Support\Facades\File;

class CsvUploaderService implements ICsvUploaderService
{
    const CSV_READ_CHUNK = 500;

    /**
     * @param $fileName
     * @return bool
     * @throws \Exception
     */
    public function upload($fileName)
    {

        if (!File::exists($fileName)) {
            throw new \Exception('There is no such file.');
        }

        $row = 1;
        if (($handle = fopen($fileName, "r")) !== FALSE) {
            while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
                $num = count($data);
                for ($c=0; $c < $num; $c++) {
                    $temp = str_getcsv($data[0], ';');
                    $staff[] =
                        [
                        'first_name' => $temp[0],
                        'last_name' => $temp[1],
                        'birthdate' => $temp[2],
                        'email' => $temp[3],
                        'home_city' => $temp[4],
                        'home_zip' => $temp[5],
                        'home_address' => $temp[6],
                        'phone' => $temp[7],
                        'company_name' => $temp[8],
                        'work_city' => $temp[9],
                        'work_address' => $temp[10],
                        'position' => $temp[11],
                        'cv'=> $temp[12]
                    ];

                }
                if ($row % self::CSV_READ_CHUNK == 0) {
                    Staff::insert($staff);
                    $staff = [];
                }
                $row++;

            }
            fclose($handle);
        }

        return true;
    }
}