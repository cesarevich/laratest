<?php

namespace App\Services\Repositories;

use App\Models\Staff;
use App\Interfaces\Repositories\IStaffRepository;
use Illuminate\Support\Facades\Input;

class StaffRepository implements IStaffRepository
{
    /**
     * Search through Staff model
     * @return mixed
     */
    public function search()
    {
        $query = Staff::query();

        if (Input::has('first_name') && !empty(Input::get('first_name'))) {
            $query = $query->where('first_name', 'like', '%' . Input::get('first_name') . '%');
        }

        if (Input::has('last_name') && !empty(Input::get('last_name'))) {
            $query = $query->where('last_name', 'like', '%' . Input::get('last_name') . '%');
        }

        return $query->paginate(10);
    }
}