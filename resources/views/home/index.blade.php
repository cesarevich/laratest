@extends('layouts.app')

@section('content')

    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <div class="row-main">
        <div class="row toggle">
            <div class="col-sm-offset-6 col-md-6">
                <button class="btn btn-info upl-btn">Upload csv</button>
            </div>
        </div>
        <div class="upload">
            <div class="row">
                <div class="col-md-12">
                    {{Form::open(['action' => 'HomeController@save', 'class' => 'form-horizontal', 'enctype' => 'multipart/form-data'])}}

                    <div class="form-group">
                        {{ Form::label('download', 'Download csv', ['class' => 'col-md-4 control-label']) }}

                        <div class="col-md-4">
                            <input type="file" name="csv_file">
                        </div>

                        <div class="col-sm-offset-2 col-md-2">
                            {{ Form::button('Upload', ['type' => 'submit', 'class' => 'btn btn-success']) }}
                        </div>
                    </div>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3">
                <aside class="side-block">
                        {{ Form::open([
                            'action' => 'HomeController@index',
                            'method' => 'get',
                            ])
                        }}
                        <dl class="dl-horizontal">
                            <dt>First Name:</dt>
                            <dd>{{ Form::text(
                                        'first_name',
                                        (isset($searchParams['first_name']) ? $searchParams['first_name'] : '').'',
                                        ['class' => 'form-control', 'id' => 'search-first-name', 'placeholder' => 'First Name...']
                                    ) }}</dd>
                        </dl>
                        <dl class="dl-horizontal">
                            <dt>Last Name:</dt>
                            <dd>{{ Form::text(
                                        'last_name',
                                        (isset($searchParams['last_name']) ? $searchParams['last_name'] : '').'',
                                        ['class' => 'form-control', 'id' => 'search-first-name', 'placeholder' => 'Last Name...']
                                    ) }}</dd>
                        </dl>
                        <dl class="dl-horizontal">
                            <dt><a href="{{ route('staffList') }}" class="btn btn-default">Reset</a></dt>
                            <dd>{{ Form::button('Search', ['type' => 'submit', 'class' => 'btn btn-default']) }}</dd>
                        </dl>
                    {{ Form::close() }}
                </aside>
            </div>

            <div class="col-md-9 employee-list">
                <div>{{ $staff->appends( $searchParams )->links() }}</div>
                @foreach($staff as $employee)
                    <div class="employee">
                        <div class="row">
                            <div class="col-md-4">
                                <dl class="dl-horizontal">
                                    <dt>First name:</dt>
                                    <dd>{{ $employee->first_name }}</dd>
                                </dl>
                                <dl class="dl-horizontal">
                                    <dt>Last name:</dt>
                                    <dd>{{ $employee->last_name }}</dd>
                                </dl>
                                <dl class="dl-horizontal">
                                    <dt>Birthdate:</dt>
                                    <dd>{{ $employee->birthdate }}</dd>
                                </dl>
                            </div>
                            <div class="col-md-8">
                                {{ $employee->cv }}
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
@endsection

@section('javascript')
    <script type="application/javascript">
        $('.upl-btn').on('click', function() {
            $('.upload').toggle();
        });
    </script>
@endsection


